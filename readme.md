Microservicio Pipeline Demo
=================

Aplicación maven que genera una imagen docker y realiza despliegue en Artifactory Registry

### Jenkins Pipeline [link-pipeline](http://207.154.193.52:8080/job/microservicio-pipeline/)

![jenkins-pipeline](./docs/pipeline.png)

### Cobertura SonarQube [link-sonar](http://207.154.193.52:9000/dashboard?id=org.springframework.samples%3Aspring-petclinic-rest)

![cobertura-sonar](./docs/sonarqube.png)

### Jfrog Artifactory Container Registry [link-artifactory](http://198.74.52.111:8082/ui/repos/tree/General/petclinic-docker-develop%2Fspring-petclinic-rest)
user/pass: admin/Admin123
![artifactory](./docs/artifactory-registry.png)
